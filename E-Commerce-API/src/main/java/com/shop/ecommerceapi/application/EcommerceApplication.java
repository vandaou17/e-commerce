package com.shop.ecommerceapi.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories({
        "com.shop.ecommerceapi.backend.repository",
        "com.shop.ecommerceapi.frontend.repository"
})
@EntityScan("com.shop.ecommerceapi.common")
@ComponentScan(basePackages = {
        "com.shop.ecommerceapi.backend",
        "com.shop.ecommerceapi.frontend",
        "com.shop.ecommerceapi.exception",
        "com.shop.ecommerceapi.configuration"
}, basePackageClasses = ApplicationStartup.class)
public class EcommerceApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(EcommerceApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(EcommerceApplication.class, args);
    }
}