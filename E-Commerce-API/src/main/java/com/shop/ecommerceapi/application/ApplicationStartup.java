package com.shop.ecommerceapi.application;

import com.shop.ecommerceapi.backend.repository.CurrencyRepository;
import com.shop.ecommerceapi.backend.repository.RoleRepository;
import com.shop.ecommerceapi.backend.repository.UserRepository;
import com.shop.ecommerceapi.backend.service.UserService;
import com.shop.ecommerceapi.common.entity.Currency;
import com.shop.ecommerceapi.common.entity.Role;
import com.shop.ecommerceapi.common.entity.RoleType;
import com.shop.ecommerceapi.common.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Component
public class ApplicationStartup implements ApplicationRunner {

    private final RoleRepository roleRepository;

    private final UserRepository userRepository;

    private final UserService userService;

    private final CurrencyRepository currencyRepository;


    @Autowired
    public ApplicationStartup(RoleRepository roleRepository,
                              UserRepository userRepository,
                              CurrencyRepository currencyRepository,
                              UserService userService) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.currencyRepository = currencyRepository;
        this.userService = userService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        addRoles();
        addUserWithAdminRole();
        addCurrenciesToDatabase();
    }

    private void addRoles() {
        if (this.roleRepository.findAll().size() > 0 ||
                !this.roleRepository.findAll().isEmpty()) {
            return;
        }

        Role admin = new Role();
        admin.setName(RoleType.ADMIN);
        admin.setDescription(admin.getDescription());

        Role shipper = new Role();
        shipper.setName(RoleType.SHIPPER);
        shipper.setDescription(shipper.getDescription());

        Role seller = new Role();
        seller.setName(RoleType.SELLER);
        seller.setDescription(seller.getDescription());

        Role editor = new Role();
        editor.setName(RoleType.EDITOR);
        editor.setDescription(editor.getDescription());

        Role assistant = new Role();
        assistant.setName(RoleType.ASSISTANT);
        assistant.setDescription(assistant.getDescription());

        roleRepository.saveAll(List.of(admin, shipper, seller, editor, assistant));
    }

    private void addUserWithAdminRole() {
        if (!userRepository.findAll().isEmpty()
                || userRepository.findAll().size() > 0){
            return;
        }

        Role role = userService.findRoleById(1).get();

        User user = new User();
        user.setFirstName("Admin");
        user.setLastName("Admin");
        user.setEmail("@Admin");
        user.setPassword("Admin123");
        user.setEnabled(true);
        user.addRole(role);

        userService.save(user);
    }

    private void addCurrenciesToDatabase() {
        if (!currencyRepository.findAll().isEmpty()
                || currencyRepository.findAll().size() > 0) {
            return;
        }

        List<Currency> currencies = Arrays.asList(
                new Currency("Cambodian Riel", "៛", "KHR"),
                new Currency("Thai baht", "฿", "THB"),
                new Currency("Vietnamese đồng ", "₫", "VND"),
                new Currency("United States Dollar", "$", "USD"),
                new Currency("British Pound", "£", "GPB"),
                new Currency("Japanese Yen", "¥", "JPY"),
                new Currency("Euro", "€", "EUR"),
                new Currency("Russian Ruble", "₽", "RUB"),
                new Currency("South Korean Won", "₩", "KRW"),
                new Currency("Chinese Yuan", "¥", "CNY"),
                new Currency("Brazilian Real", "R$", "BRL"),
                new Currency("Australian Dollar", "$", "AUD"),
                new Currency("Canadian Dollar", "$", "CAD"),
                new Currency("Indian Rupee", "₹", "INR")
        );

        currencyRepository.saveAll(currencies);
    }
}
