package com.shop.ecommerceapi.configuration.pagination;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Pageable {
    private int pageNumber;
    private int pageSize;
    private Sort sort;
    private Map<String, Object> filter = new HashMap<>();

    public Pageable() {
    }

    public Pageable(int pageNumber, int pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }

    public Pageable(int pageNumber, int pageSize, Sort sort) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.sort = sort;
    }

    public Pageable(int pageNumber, int pageSize, Sort sort, Map<String, Object> filter) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.sort = sort;
        this.filter = filter;
    }

    public enum Sort {
        ASC, DESC;
    }
}
