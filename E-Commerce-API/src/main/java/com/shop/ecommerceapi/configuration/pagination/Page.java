package com.shop.ecommerceapi.configuration.pagination;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Page<T> {

    private List<T> content = new ArrayList<>();

    private Integer totalPages;

    private Long totalElements;

    private Integer currentPage;

    private Integer lastPage;

    public Page() {
    }

    public Page(Long totalElements, Integer currentPage, int pageSize) {
        this.totalElements = totalElements;
        this.currentPage = currentPage;
        generateLastPageAndTotalPage(pageSize);
    }

    public Page(List<T> content, Long totalElements, Integer currentPage, int pageSize) {
        this.content = content;
        this.totalElements = totalElements;
        this.currentPage = currentPage;
        generateLastPageAndTotalPage(pageSize);
    }

    private void generateLastPageAndTotalPage(int pageSize) {
        this.totalPages = (int) (Math.ceil((double)totalElements / pageSize));
        this.lastPage = totalPages;
    }

}
