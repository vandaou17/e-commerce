package com.shop.ecommerceapi.common.entity;

import com.shop.ecommerceapi.common.IdBasedEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "reviews_votes")
@Getter
@Setter
public class ReviewVote extends IdBasedEntity {

    private int vote;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "review_id")
    private Review review;
}
