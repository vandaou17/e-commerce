package com.shop.ecommerceapi.common.entity;

public enum AuthenticationType {
    DATABASE,
    GOOGLE,
    FACEBOOK
}
