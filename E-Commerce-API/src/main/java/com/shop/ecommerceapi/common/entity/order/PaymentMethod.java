package com.shop.ecommerceapi.common.entity.order;
public enum PaymentMethod {
    COD,
    CREDIT_CARD,
    PAYPAL
}
