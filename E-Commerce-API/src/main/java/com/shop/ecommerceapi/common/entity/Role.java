package com.shop.ecommerceapi.common.entity;

import com.shop.ecommerceapi.common.IdBasedEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "roles")
@Getter
@Setter
public class Role extends IdBasedEntity {

    @Column(length = 40, nullable = false, unique = true)
    @Enumerated(value = EnumType.STRING)
    private RoleType name;

    @Column(length = 150, nullable = false)
    private String description;

    public String getDescription() {
        if (this.name == RoleType.ADMIN) return "Manage everything";
        else if (this.name == RoleType.ASSISTANT)
            return "Manage questions and reviews";
        else if (this.name == RoleType.SELLER)
            return "Manage product price, customers, shipping, orders and sales report";
        else if (this.name == RoleType.EDITOR)
            return "Manage categories, brands, products, articles and menus";
        else if (this.name == RoleType.SHIPPER)
            return "View products, view orders and update order status";

        return "";
    }
}
