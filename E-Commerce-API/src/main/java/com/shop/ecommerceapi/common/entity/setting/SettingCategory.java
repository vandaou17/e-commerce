package com.shop.ecommerceapi.common.entity.setting;

public enum SettingCategory {
    GENERAL,
    MAIL_SERVER,
    MAIL_TEMPLATES,
    CURRENCY,
    PAYMENT
}
