package com.shop.ecommerceapi.common.entity.order;

import com.shop.ecommerceapi.common.IdBasedEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "order_tracks")
@Getter
@Setter
public class OrderTrack extends IdBasedEntity {

    private String note;

    private Date updatedTime;

    @Enumerated(EnumType.STRING)
    @Column(length = 45, nullable = false)
    private OrderStatus status;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;
}
