package com.shop.ecommerceapi.common;

import com.shop.ecommerceapi.common.entity.Country;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class AbstractAddressWithCountry extends AbstractAddress {

    @ManyToOne
    @JoinColumn(name = "country_id")
    protected Country country;

}
