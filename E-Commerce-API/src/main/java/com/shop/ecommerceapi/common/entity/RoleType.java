package com.shop.ecommerceapi.common.entity;

public enum RoleType {
    ADMIN,
    SELLER,
    EDITOR,
    SHIPPER,
    ASSISTANT
}
