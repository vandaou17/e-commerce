package com.shop.ecommerceapi.common.entity;

import com.shop.ecommerceapi.common.IdBasedEntity;
import com.shop.ecommerceapi.common.entity.product.Product;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "reviews")
@Getter
@Setter
public class Review extends IdBasedEntity {

    @Column(length = 128, nullable = false)
    private String headLine;

    @Column(length = 300, nullable = false)
    private String comment;

    private int rating;

    private Integer vote;

    @Column(nullable = false)
    private Date reviewTime;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
}
