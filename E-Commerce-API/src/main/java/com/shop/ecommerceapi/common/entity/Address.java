package com.shop.ecommerceapi.common.entity;

import com.shop.ecommerceapi.common.AbstractAddressWithCountry;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "addresses")
@Getter
@Setter
public class Address extends AbstractAddressWithCountry {

    private boolean defaultForShipping;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;


}
