package com.shop.ecommerceapi.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shop.ecommerceapi.common.IdBasedEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "countries")
@Getter
@Setter
public class Country extends IdBasedEntity {

    @Column(nullable = false, length = 45)
    private String name;

    @Column(nullable = false, length = 5)
    private String code;

    @JsonIgnore
    @OneToMany(mappedBy = "country")
    private Set<State> states;
}
