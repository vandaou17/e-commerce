package com.shop.ecommerceapi.common.entity.product;

import com.shop.ecommerceapi.common.IdBasedEntity;
import com.shop.ecommerceapi.common.entity.Brand;
import com.shop.ecommerceapi.common.entity.Category;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Entity
@Table(name = "products")
@Getter
@Setter
@NoArgsConstructor
public class Product extends IdBasedEntity {

    @Column(unique = true, nullable = false)
    private String name;

    @Column(unique = true, nullable = false)
    private String alias;

    @Column(length = 512, nullable = false)
    private String shortDescription;

    @Column(length = 4096, nullable = false)
    private String fullDescription;

    @Column(nullable = false, updatable = false)
    private Date createdTime;

    private Date updatedTime;

    private boolean enabled;

    private boolean inStock;

    private float cost;

    private float price;

    private float discountPercent;

    private float length;

    private float width;

    private float height;

    private float weight;

    private Integer reviewCount;

    private Float averageRating;

    @Column(nullable = false)
    private String mainImage;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProductImage> images = new HashSet<>();

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProductDetail> details = new ArrayList<>();

    public Product(String name, String alias, String shortDescription,
                   String fullDescription,
                   float cost, float price, float discountPercent,
                   float length, float width, float height,
                   float weight, Category category, Brand brand) {
        this.name = name;
        this.alias = alias;
        this.shortDescription = shortDescription;
        this.fullDescription = fullDescription;
        this.enabled = true;
        this.inStock = true;
        this.createdTime = new Date();
        this.cost = cost;
        this.price = price;
        this.discountPercent = discountPercent;
        this.length = length;
        this.width = width;
        this.height = height;
        this.weight = weight;
        this.category = category;
        this.brand = brand;
    }
}
