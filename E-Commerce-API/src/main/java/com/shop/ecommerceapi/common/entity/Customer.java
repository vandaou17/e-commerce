package com.shop.ecommerceapi.common.entity;

import com.shop.ecommerceapi.common.AbstractAddressWithCountry;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "customers")
@Getter
@Setter
public class Customer extends AbstractAddressWithCountry {

    @Column(nullable = false, unique = true, length = 45)
    private String email;

    @Column(nullable = false, length = 64)
    private String password;

    @Column(length = 64)
    private String verificationCode;

    private boolean enabled;

    @Column(nullable = false, updatable = false)
    private Date createdTime;

    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private AuthenticationType authenticationType;

    @Column(length = 30)
    private String resetPasswordToken;
}
