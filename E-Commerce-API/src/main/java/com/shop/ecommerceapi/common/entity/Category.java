package com.shop.ecommerceapi.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shop.ecommerceapi.common.IdBasedEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "categories")
@Getter
@Setter
public class Category extends IdBasedEntity {

    @Column(length = 128, nullable = false, unique = true)
    private String name;

    @Column(length = 64, nullable = false, unique = true)
    private String alias;

    @Column(length = 128, nullable = false)
    private String image;

    private boolean enabled;

    @Column(name = "all_parent_ids", length = 256)
    private String allParentIds;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Category parent;

    @OneToMany(mappedBy = "parent")
    @OrderBy("name asc")
    private Set<Category> children = new HashSet<>();

    public static Category copyIdAndName(Integer id, String name) {
        Category cloneCategory = new Category();
        cloneCategory.setId(id);
        cloneCategory.setName(name);
        return cloneCategory;
    }

}
