package com.shop.ecommerceapi.frontend.service.impl;

import com.shop.ecommerceapi.common.entity.Category;
import com.shop.ecommerceapi.common.entity.product.Product;
import com.shop.ecommerceapi.exception.NotFoundException;
import com.shop.ecommerceapi.frontend.repository.CategoryRepository;
import com.shop.ecommerceapi.frontend.repository.ProductRepository;
import com.shop.ecommerceapi.frontend.service.ProductService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ProductService")
public class ProductServiceImpl implements ProductService {

    public static final int PRODUCTS_PER_PAGE = 10;

    private ProductRepository productRepository;

    private CategoryRepository categoryRepository;

    public ProductServiceImpl(ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Page<Product> search(String keyword, int pageNum) {
        Pageable pageable = PageRequest.of(pageNum - 1, PRODUCTS_PER_PAGE);
        if (keyword != null)
            return productRepository.search(keyword, pageable);

        return productRepository.findAll(pageable);
    }

    @Override
    public Page<Product> listByCategoryId(int pageNum, Integer categoryId) {
        if (!categoryRepository.existsById(categoryId))
            throw new NotFoundException("Not found Category with specific ID");

        Pageable pageable = PageRequest.of(pageNum - 1, PRODUCTS_PER_PAGE);

        return productRepository.listByCategory(categoryId, pageable);
    }

    @Override
    public Product getProductById(Integer id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found Category with specific ID"));

        return product;
    }
}
