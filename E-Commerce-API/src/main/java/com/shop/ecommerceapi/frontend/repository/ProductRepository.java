package com.shop.ecommerceapi.frontend.repository;

import com.shop.ecommerceapi.common.entity.product.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository("ProductRepo")
public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query("SELECT p FROM Product p WHERE p.enabled = true AND p.category.id = ?1 order by p.name ASC")
    Page<Product> listByCategory(Integer id, Pageable pageable);

    @Query("SELECT p FROM Product p WHERE p.enabled = true AND p.name LIKE ?1% OR " +
            "p.shortDescription LIKE ?1% OR " +
            "p.fullDescription LIKE ?1%")
    Page<Product> search(String keyword, Pageable pageable);
}
