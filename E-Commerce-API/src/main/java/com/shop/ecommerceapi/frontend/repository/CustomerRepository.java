package com.shop.ecommerceapi.frontend.repository;

import com.shop.ecommerceapi.common.entity.AuthenticationType;
import com.shop.ecommerceapi.common.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository("CustomerRepo")
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    @Query("SELECT c FROM Customer c WHERE c.email = ?1")
    Optional<Customer> findByEmail(String email);

    @Query("SELECT c FROM Customer c WHERE c.verificationCode = ?1")
    Optional<Customer> findByVerificationCode(String code);

    @Query("SELECT c FROM Customer c WHERE c.resetPasswordToken = ?1")
    Optional<Customer> findByResetPasswordToken(String token);

    @Query("UPDATE Customer c SET c.enabled = TRUE, c.verificationCode = null WHERE c.id = ?1")
    @Modifying
    void enable(Integer id);

    @Query("UPDATE Customer c SET c.authenticationType = ?2 WHERE c.id = ?1")
    @Modifying
    void updateAuthenticationType(Integer customerId, AuthenticationType type);
}
