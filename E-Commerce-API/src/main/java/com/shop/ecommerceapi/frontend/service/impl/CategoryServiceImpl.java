package com.shop.ecommerceapi.frontend.service.impl;

import com.shop.ecommerceapi.common.entity.Category;
import com.shop.ecommerceapi.exception.NotFoundException;
import com.shop.ecommerceapi.frontend.repository.CategoryRepository;
import com.shop.ecommerceapi.frontend.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("CategoryService")
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> listNoChildrenCategories() {
        List<Category> categories = categoryRepository.findAllEnabled();
        if (categories.size() == 0 || categories.isEmpty())
            throw new NotFoundException("Not found Category in database");

        return categories;
    }

    @Override
    public List<Category> listChildrenByCategoryId(Integer id) {
        List<Category> categories = categoryRepository.findAllEnabledByParentId(id);
        if (categories.size() == 0 || categories.isEmpty())
            throw new NotFoundException("Not found Category in database");

        return categories;
    }

    @Override
    public Category getCategoryByAlias(String keyword) {
        Category category = categoryRepository.findByKeyword(keyword)
                .orElseThrow(() -> new NotFoundException("Not found Category with specific keyword"));

        return category;
    }
}
