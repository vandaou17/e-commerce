package com.shop.ecommerceapi.frontend.service;

import com.shop.ecommerceapi.common.entity.product.Product;
import org.springframework.data.domain.Page;

public interface ProductService {

    Page<Product> search(String keyword, int pageNum);

    Page<Product> listByCategoryId(int pageNum, Integer categoryId);

    Product getProductById(Integer id);
}
