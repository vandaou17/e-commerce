package com.shop.ecommerceapi.frontend.service;

import com.shop.ecommerceapi.common.entity.Category;

import java.util.List;

public interface CategoryService {

    List<Category> listNoChildrenCategories();

    List<Category> listChildrenByCategoryId(Integer id);

    Category getCategoryByAlias(String keyword);
}
