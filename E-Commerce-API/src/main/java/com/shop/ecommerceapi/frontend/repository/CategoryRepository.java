package com.shop.ecommerceapi.frontend.repository;

import com.shop.ecommerceapi.common.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("CategoryRepo")
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @Query("SELECT c FROM Category c WHERE c.enabled = true ORDER BY c.name ASC")
    List<Category> findAllEnabled();

    @Query("SELECT c FROM Category c WHERE c.enabled = true AND c.alias = ?1")
    Optional<Category> findByKeyword(String keyword);

    @Query("SELECT c FROM Category c WHERE c.parent.id = ?1 ORDER BY c.name ASC")
    List<Category> findAllEnabledByParentId(Integer id);


}
