package com.shop.ecommerceapi.frontend.service;

import com.shop.ecommerceapi.common.entity.AuthenticationType;
import com.shop.ecommerceapi.common.entity.Country;
import com.shop.ecommerceapi.common.entity.Customer;

import java.util.List;

public interface CustomerService {

    List<Country> listAllCountries();

    void register(Customer customer);

    Customer getUserByEmail(String email);

    boolean verify(String verification);

    Customer addNewWithOAuth(String name, String email, String countryCode, AuthenticationType authenticationType);

    Customer update(Customer customer);

    Customer getByResetPasswordToken(String token);

    void updateAuthenticationType(Customer customer, AuthenticationType authenticationType);
}
