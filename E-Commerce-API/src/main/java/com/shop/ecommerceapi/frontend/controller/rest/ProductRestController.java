package com.shop.ecommerceapi.frontend.controller.rest;

import com.shop.ecommerceapi.backend.payload.response.PagingResponse;
import com.shop.ecommerceapi.common.entity.product.Product;
import com.shop.ecommerceapi.common.entity.product.ProductImage;
import com.shop.ecommerceapi.frontend.payload.response.ProductResponse;
import com.shop.ecommerceapi.frontend.service.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController("ProductRest")
@RequestMapping("/api/frontend/products")
public class ProductRestController {

    private ProductService productService;
    
    private ModelMapper mapper;

    public ProductRestController(ProductService productService, ModelMapper mapper) {
        this.productService = productService;
        this.mapper = mapper;
    }

    @GetMapping("/page/{pageNum}")
    public ResponseEntity<PagingResponse> listProductByPage(@PathVariable int pageNum,
                                               @RequestParam Integer categoryId) {
        Page<Product> productPage = productService.listByCategoryId(pageNum, categoryId);
        return getPagingResponseResponseEntity(productPage);
    }

    @GetMapping("/page/{pageNum}/search")
    public ResponseEntity<PagingResponse> searchProductByPage(@PathVariable int pageNum,
                                                              @RequestParam(required = false) String keyword) {
        Page<Product> productPage = productService.search(keyword, pageNum);
        return getPagingResponseResponseEntity(productPage);
    }

    @GetMapping("/{id}")
    public ResponseEntity viewProductDetail(@PathVariable Integer id) {
        Product product = productService.getProductById(id);
        Set<String> productImages = new HashSet<>();
        for (ProductImage image : product.getImages()) {
            productImages.add(image.getName());
        }

        ProductResponse productResponse = mapper.map(product, ProductResponse.class);
        productResponse.setImageNames(productImages);

        return new ResponseEntity<>(productResponse, HttpStatus.FOUND);
        
    }

    private ResponseEntity<PagingResponse> getPagingResponseResponseEntity(Page<Product> productPage) {
        List<Product> contents = productPage.getContent();

        List<ProductResponse> productResponses = new ArrayList<>();
        Set<String> productImages = new HashSet<>();
        for (Product content : contents) {
            for (ProductImage image :content.getImages()) {
                productImages.add(image.getName());
            }
            ProductResponse productResponse = mapper.map(content, ProductResponse.class);
            productResponse.setImageNames(productImages);
            productResponses.add(productResponse);
        }

        PagingResponse<ProductResponse> productResponsePage = mapper.map(productPage, PagingResponse.class);
        productResponsePage.getContent().clear();
        productResponsePage.getContent().addAll(productResponses);

        return new ResponseEntity<>(productResponsePage, HttpStatus.OK);
    }
}
