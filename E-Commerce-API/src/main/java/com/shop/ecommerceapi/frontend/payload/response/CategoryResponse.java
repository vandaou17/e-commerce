package com.shop.ecommerceapi.frontend.payload.response;

import lombok.Data;

@Data
public class CategoryResponse {

    private int id;

    private String name;

    private String alias;

    private String image;

    private boolean enabled;

}
