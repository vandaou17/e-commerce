package com.shop.ecommerceapi.frontend.security;

import com.shop.ecommerceapi.common.entity.Customer;
import com.shop.ecommerceapi.exception.NotFoundException;
import com.shop.ecommerceapi.frontend.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("CustomerServiceLoadUser")
public class CustomerServiceImpl implements UserDetailsService {

    @Autowired
    private CustomerRepository customerRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = customerRepository.findByEmail(username)
                .orElseThrow(() -> new NotFoundException("No customer found with the email " + username));

        return new CustomerDetails(customer);
    }
}
