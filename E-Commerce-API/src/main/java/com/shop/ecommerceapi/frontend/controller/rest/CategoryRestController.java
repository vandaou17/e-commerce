package com.shop.ecommerceapi.frontend.controller.rest;

import com.shop.ecommerceapi.common.entity.Category;
import com.shop.ecommerceapi.frontend.payload.response.CategoryResponse;
import com.shop.ecommerceapi.frontend.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController("CategoryRest")
@RequestMapping("/api/frontend/categories")
public class CategoryRestController {

    private CategoryService categoryService;

    private ModelMapper mapper;

    public CategoryRestController(CategoryService categoryService, ModelMapper mapper) {
        this.categoryService = categoryService;
        this.mapper = mapper;
    }

    @GetMapping
    public ResponseEntity listCategories() {
        List<Category> categories = categoryService.listNoChildrenCategories();

        List<CategoryResponse> categoryResponses = new ArrayList<>();
        for (Category category : categories) {
            categoryResponses.add(mapper.map(category, CategoryResponse.class));
        }

        return new ResponseEntity<>(categoryResponses, HttpStatus.OK);
    }

    @GetMapping("/{parentId}")
    public ResponseEntity listChildrenByParentId(@PathVariable Integer parentId) {
        List<Category> categories = categoryService.listChildrenByCategoryId(parentId);

        List<CategoryResponse> categoryResponses = new ArrayList<>();
        for (Category category : categories) {
            categoryResponses.add(mapper.map(category, CategoryResponse.class));
        }

        return new ResponseEntity<>(categoryResponses, HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity searchCategory(@RequestParam String keyword) {
        Category category = categoryService.getCategoryByAlias(keyword);

        CategoryResponse categoryResponse = mapper.map(category, CategoryResponse.class);

        return new ResponseEntity(categoryResponse, HttpStatus.FOUND);
    }
}
