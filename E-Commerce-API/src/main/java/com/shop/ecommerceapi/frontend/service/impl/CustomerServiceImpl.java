package com.shop.ecommerceapi.frontend.service.impl;

import com.shop.ecommerceapi.backend.repository.CountryRepository;
import com.shop.ecommerceapi.common.entity.AuthenticationType;
import com.shop.ecommerceapi.common.entity.Country;
import com.shop.ecommerceapi.common.entity.Customer;
import com.shop.ecommerceapi.exception.NotFoundException;
import com.shop.ecommerceapi.frontend.repository.CustomerRepository;
import com.shop.ecommerceapi.frontend.service.CustomerService;
import org.modelmapper.internal.bytebuddy.utility.RandomString;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("CustomerService")
public class CustomerServiceImpl implements CustomerService {

    private CountryRepository countryRepository;

    private CustomerRepository customerRepository;

    private PasswordEncoder passwordEncoder;

    public CustomerServiceImpl(CountryRepository countryRepository,
                               CustomerRepository customerRepository,
                               PasswordEncoder passwordEncoder) {
        this.countryRepository = countryRepository;
        this.customerRepository = customerRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<Country> listAllCountries() {
        return countryRepository.findAllByOrderByNameAsc();
    }

    @Override
    public void register(Customer customer) {
        String encodedPassword = passwordEncoder.encode(customer.getPassword());
        customer.setPassword(encodedPassword);
        customer.setEnabled(false);
        customer.setCreatedTime(new Date());
        customer.setAuthenticationType(AuthenticationType.DATABASE);

        String randomCode = RandomString.make(64);
        customer.setVerificationCode(randomCode);

        customerRepository.save(customer);
    }

    @Override
    public Customer getUserByEmail(String email) {
        return customerRepository.findByEmail(email)
                .orElseThrow(() -> new NotFoundException("Not found customer with specific ID"));

    }

    @Override
    public boolean verify(String verification) {
        Customer customer = customerRepository.findByVerificationCode(verification).get();
        if (customer == null || customer.isEnabled())
            return false;

        customerRepository.enable(customer.getId());
        return true;
    }

    @Override
    public Customer addNewWithOAuth(String name, String email, String countryCode,
                                    AuthenticationType authenticationType) {
        Customer customer = new Customer();
        customer.setEmail(email);
        setName(name, customer);
        customer.setEnabled(true);
        customer.setCreatedTime(new Date());
        customer.setAuthenticationType(authenticationType);
        customer.setCountry(countryRepository.findByCode(countryCode).get());

        return customerRepository.save(customer);
    }

    @Override
    public Customer update(Customer customer) {
        Customer customerDB = customerRepository.findById(customer.getId())
                .orElseThrow(() -> new NotFoundException("Not found Customer with specific ID"));

        customerDB.setFirstName(customer.getFirstName());
        customerDB.setLastName(customer.getLastName());
        customerDB.setEmail(customer.getEmail());
        customerDB.setPassword(passwordEncoder.encode(customer.getPassword()));
        customerDB.setPhoneNumber(customer.getPhoneNumber());
        customerDB.setAddressLine1(customer.getAddressLine1());
        customerDB.setAddressLine2(customer.getAddressLine2());
        customerDB.setCity(customer.getCity());
        customerDB.setCountry(customer.getCountry());
        customerDB.setState(customer.getState());
        customerDB.setPostalCode(customer.getPostalCode());

        return customerRepository.save(customerDB);
    }

    @Override
    public Customer getByResetPasswordToken(String token) {
        return customerRepository.findByResetPasswordToken(token)
                .orElseThrow(() -> new NotFoundException("Not found Customer with invalid token"));
    }

    @Override
    public void updateAuthenticationType(Customer customer, AuthenticationType authenticationType) {
        if (!customer.getAuthenticationType().equals(authenticationType))
            customerRepository.updateAuthenticationType(customer.getId(), authenticationType);
    }

    private void setName(String name, Customer customer) {
        String[] nameArray = name.split(" ");
        if (nameArray.length < 2) {
            customer.setFirstName(name);
            customer.setLastName("");
        } else {
            String firstName = nameArray[0];
            customer.setFirstName(firstName);

            String lastName = name.replaceFirst(firstName + " ", "");
            customer.setLastName(lastName);
        }
    }

}
