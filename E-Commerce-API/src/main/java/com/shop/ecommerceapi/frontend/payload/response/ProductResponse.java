package com.shop.ecommerceapi.frontend.payload.response;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class ProductResponse {

    private int id;

    private String name;

    private float averageRating;

    private float price;

    private String shortDescription;

    private String fullDescription;

    private String brandName;

    private int brandId;

    private boolean inStock;

    private String mainImage;

    private Set<String> imageNames;
}
