package com.shop.ecommerceapi.frontend.security.oauth;

import com.shop.ecommerceapi.common.entity.AuthenticationType;
import com.shop.ecommerceapi.common.entity.Customer;
import com.shop.ecommerceapi.frontend.service.CustomerService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class OAuth2LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private CustomerService customerService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws ServletException, IOException {
        CustomerOAuth2User oAuth2User = (CustomerOAuth2User) authentication.getPrincipal();

        String name = oAuth2User.getName();
        String email = oAuth2User.getEmail();
        String countryCode = request.getLocale().getCountry();
        String clientName = oAuth2User.getClientName();

        AuthenticationType authenticationType = getAuthenticationType(clientName);

        Customer customer = customerService.getUserByEmail(email);
        if (customer == null)
            customerService.addNewWithOAuth(name, email, countryCode, authenticationType);
        else {
            oAuth2User.setFullName(customer.getFirstName() + " " + customer.getLastName());
            customerService.updateAuthenticationType(customer, authenticationType);
        }

        super.onAuthenticationSuccess(request, response, authentication);
    }

    private AuthenticationType getAuthenticationType(String clientName) {
        if (clientName.equals("Google"))
            return AuthenticationType.GOOGLE;
        else if (clientName.equals("Facebook"))
            return AuthenticationType.FACEBOOK;
        else return AuthenticationType.DATABASE;
    }
}
