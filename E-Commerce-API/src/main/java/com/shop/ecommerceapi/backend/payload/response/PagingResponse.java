package com.shop.ecommerceapi.backend.payload.response;

import lombok.*;

import java.util.List;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PagingResponse<T> {

    private boolean first;

    private boolean last;

    private int totalPages;

    private int totalElements;

    private int size;

    private int number;

    private int numberOfElements;

    private List<T> content;

}
