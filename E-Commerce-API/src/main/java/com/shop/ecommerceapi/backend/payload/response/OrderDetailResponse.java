package com.shop.ecommerceapi.backend.payload.response;


import com.shop.ecommerceapi.backend.payload.response.order.OverviewResponse;
import com.shop.ecommerceapi.backend.payload.response.order.ProductResponse;
import com.shop.ecommerceapi.backend.payload.response.order.ShippingResponse;
import com.shop.ecommerceapi.backend.payload.response.order.TrackResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailResponse {

    private OverviewResponse overview;

    private Set<ProductResponse> products;

    private ShippingResponse shipping;

    private List<TrackResponse> tracks;
}
