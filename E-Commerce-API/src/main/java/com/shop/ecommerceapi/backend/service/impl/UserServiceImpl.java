package com.shop.ecommerceapi.backend.service.impl;

import com.shop.ecommerceapi.backend.repository.RoleRepository;
import com.shop.ecommerceapi.backend.repository.UserRepository;
import com.shop.ecommerceapi.backend.service.UserService;
import com.shop.ecommerceapi.common.entity.Role;
import com.shop.ecommerceapi.common.entity.User;
import com.shop.ecommerceapi.exception.ConflictException;
import com.shop.ecommerceapi.exception.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    public static final int USERS_PER_PAGE = 5;

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User save(User value) {
        if (isEmailUnique(value.getEmail()))
            throw new ConflictException("A user with email was conflict");

        value.setPassword(passwordEncoder.encode(value.getPassword()));

        return userRepository.save(value);
    }

    @Override
    public User update(User value, Integer id) {
        User oldUser = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A user with the specified ID was not found"));

        oldUser.setPassword(passwordEncoder.encode(value.getPassword()));

        return userRepository.save(oldUser);
    }

    @Override
    public User delete(User value) {
        User user = userRepository.findById(value.getId())
                .orElseThrow(() -> new NotFoundException("A user with the specified ID was not found"));
        userRepository.delete(user);

        return user;
    }

    @Override
    public void deleteById(Integer id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A user with the specified ID was not found"));

        userRepository.deleteById(user.getId());
    }

    @Override
    public List<User> findAll() {
        if (userIsEmpty()) {
            throw new NotFoundException("User empty list in database");
        }

        List<User> users = userRepository.findAll();
        return users.stream().toList();
    }

    @Override
    public Optional<User> findById(Integer id) {
        if (userRepository.findById(id).isEmpty())
            throw new NotFoundException("A user with the specified ID was not found");

        User user = userRepository.findById(id).get();
        return Optional.of(user);
    }

    @Override
    public List<Role> findAllRole() {
        if (userIsEmpty())
            throw new NotFoundException("Role empty list in database");

        return roleRepository.findAll().stream().toList();
    }

    @Override
    public Optional<Role> findRoleById(Integer id) {
        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A role with the specified ID was not found"));

        return Optional.ofNullable(role);
    }

    @Override
    public User findByEmail(String email) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new NotFoundException("User does not exist"));

        return user;
    }

    @Override
    public Page<User> listByPage(int pageNum, String keyword) {
        if (userIsEmpty()) {
            throw new NotFoundException("User empty list in database");
        }

        Pageable pageable = PageRequest.of(pageNum - 1, USERS_PER_PAGE);
        if (keyword != null)
            return userRepository.findAllByKeyword(keyword, pageable);

        return userRepository.findAll(pageable);
    }

    private boolean isEmailUnique(String email) {
        return userRepository.existsByEmail(email);
    }

    private boolean userIsEmpty() {
        return userRepository.findAll().size() == 0
                || userRepository.findAll().isEmpty();
    }
}
