package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.order.Order;
import com.shop.ecommerceapi.common.entity.order.OrderDetail;

public interface OrderDetailService {

    Order viewDetail(Integer id);

}
