package com.shop.ecommerceapi.backend.service.impl;

import com.shop.ecommerceapi.backend.repository.ReviewRepository;
import com.shop.ecommerceapi.backend.service.ReviewService;
import com.shop.ecommerceapi.common.entity.Review;
import com.shop.ecommerceapi.exception.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ReviewServiceImpl implements ReviewService {

    public static final int REVIEWS_PER_PAGE = 5;

    private ReviewRepository reviewRepository;

    public ReviewServiceImpl(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @Override
    public Page<Review> listByPage(int pageNum, String keyword) {
        Pageable pageable = PageRequest.of(pageNum - 1, REVIEWS_PER_PAGE);
        if (keyword != null)
            return reviewRepository.findAllByKeyword(keyword, pageable);

        return reviewRepository.findAll(pageable);
    }

    @Override
    public Review findById(Integer id) {
        Review review = reviewRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found order with specific ID"));

        return review;
    }

    @Override
    public void deleteById(Integer id) {
        Review review = reviewRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found order with specific ID"));

        reviewRepository.deleteById(review.getId());
    }
}
