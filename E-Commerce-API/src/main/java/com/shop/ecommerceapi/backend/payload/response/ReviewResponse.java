package com.shop.ecommerceapi.backend.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReviewResponse {

    private int id;

    private String product;

    private String customerName;

    private String headline;

    private int rating;

    private String comment;

    private Date reviewTime;
}
