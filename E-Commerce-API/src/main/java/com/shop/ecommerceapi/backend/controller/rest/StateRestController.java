package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.service.CountryService;
import com.shop.ecommerceapi.backend.service.StateService;
import com.shop.ecommerceapi.common.entity.Country;
import com.shop.ecommerceapi.common.entity.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/states")
public class StateRestController {

    @Autowired
    private StateService stateService;

    @Autowired
    private CountryService countryService;

    @GetMapping("/{id}")
    public ResponseEntity<List<State>> listByCountryId(@PathVariable Integer id) {
        List<State> states = stateService.findByCountry(id);

        return new ResponseEntity<>(states, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<State> editState(@PathVariable Integer id,
                                           @RequestParam String name) {
        State state = stateService.findById(id);
        state.setName(name);

        State updatedState = stateService.save(state);

        return new ResponseEntity<>(updatedState, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<State> deleteById(@PathVariable Integer id) {
        stateService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/{countryId}")
    public ResponseEntity<State> addNewState(@PathVariable Integer countryId,
                                             @RequestParam String name) {
        Country country = countryService.findById(countryId).get();

        State state = new State();
        state.setName(name);
        state.setCountry(country);

        State savedState = stateService.save(state);
        return new ResponseEntity<>(savedState, HttpStatus.CREATED);
    }
}
