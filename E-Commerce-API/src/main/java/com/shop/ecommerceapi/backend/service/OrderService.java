package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.order.Order;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    Page<Order> listByPage(int pageNum, String keyword);

    void deleteById(Integer id);

    Optional<Order> findById(Integer id);

    Order update(Order value, Integer id);
}
