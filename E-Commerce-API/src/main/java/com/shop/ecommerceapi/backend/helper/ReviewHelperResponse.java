package com.shop.ecommerceapi.backend.helper;

import com.shop.ecommerceapi.backend.payload.response.ReviewResponse;
import com.shop.ecommerceapi.common.entity.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

public class ReviewHelperResponse {

    public static Page<ReviewResponse> reviewResponses(Page<Review> reviews) {
        List<ReviewResponse> reviewResponses = new ArrayList<>();

        for (Review review : reviews) {
            reviewResponses.add(new ReviewResponse(
                    review.getId(),
                    review.getProduct().getName(),
                    review.getCustomer().getFirstName() + " " + review.getCustomer().getLastName(),
                    review.getHeadLine(),
                    review.getRating(),
                    review.getComment(),
                    review.getReviewTime()
            ));
        }

        Page<ReviewResponse> reviewResponsePage = new Page<>() {
            @Override
            public int getTotalPages() {
                return reviews.getTotalPages();
            }

            @Override
            public long getTotalElements() {
                return reviews.getTotalElements();
            }

            @Override
            public <U> Page<U> map(Function<? super ReviewResponse, ? extends U> converter) {
                return null;
            }

            @Override
            public int getNumber() {
                return reviews.getNumber();
            }

            @Override
            public int getSize() {
                return reviews.getSize();
            }

            @Override
            public int getNumberOfElements() {
                return reviews.getNumberOfElements();
            }

            @Override
            public List<ReviewResponse> getContent() {
                return reviewResponses;
            }

            @Override
            public boolean hasContent() {
                return reviews.hasContent();
            }

            @Override
            public Sort getSort() {
                return reviews.getSort();
            }

            @Override
            public boolean isFirst() {
                return reviews.isFirst();
            }

            @Override
            public boolean isLast() {
                return reviews.isLast();
            }

            @Override
            public boolean hasNext() {
                return reviews.hasNext();
            }

            @Override
            public boolean hasPrevious() {
                return reviews.hasPrevious();
            }

            @Override
            public Pageable nextPageable() {
                return reviews.nextPageable();
            }

            @Override
            public Pageable previousPageable() {
                return reviews.previousPageable();
            }

            @Override
            public Iterator<ReviewResponse> iterator() {
                return iterator();
            }
        };

        return reviewResponsePage;
    }
}
