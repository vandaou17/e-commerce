package com.shop.ecommerceapi.backend.payload.response.order;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {

    private int quantity;

    private float productCost;

    private float shippingCost;

    private float unitPrice;

    private float subTotal;

    private Integer productId;


}
