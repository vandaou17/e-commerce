package com.shop.ecommerceapi.backend.service.impl;

import com.shop.ecommerceapi.backend.repository.CountryRepository;
import com.shop.ecommerceapi.backend.repository.StateRepository;
import com.shop.ecommerceapi.backend.service.StateService;
import com.shop.ecommerceapi.common.entity.Country;
import com.shop.ecommerceapi.common.entity.State;
import com.shop.ecommerceapi.exception.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StateServiceImpl implements StateService {

    private StateRepository stateRepository;

    private CountryRepository countryRepository;

    public StateServiceImpl(StateRepository stateRepository, CountryRepository countryRepository) {
        this.stateRepository = stateRepository;
        this.countryRepository = countryRepository;
    }

    @Override
    public List<State> findByCountry(Integer countryId) {
        Country country = countryRepository.findById(countryId)
                .orElseThrow(() -> new NotFoundException("Not found country with specific ID"));

        List<State> states = stateRepository.findByCountryOrderByNameAsc(country);

        return states.stream().toList();
    }

    @Override
    public void deleteById(Integer id) {
        stateRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Cannot delete with specific ID"));

        stateRepository.deleteById(id);
    }

    @Override
    public State save(State value) {
        return stateRepository.save(value);
    }

    @Override
    public State findById(Integer id) {
        State state = stateRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found state/province with specific ID"));

        return state;
    }
}
