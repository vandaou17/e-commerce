package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.Country;

import java.util.List;

public interface CountryService extends BaseService<Country> {

}
