package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.FileUploadUtil;
import com.shop.ecommerceapi.backend.service.BrandService;
import com.shop.ecommerceapi.backend.service.CategoryService;
import com.shop.ecommerceapi.common.entity.Brand;
import com.shop.ecommerceapi.common.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/brands")
public class BrandRestController {

    private BrandService brandService;

    private CategoryService categoryService;

    public BrandRestController(BrandService brandService, CategoryService categoryService) {
        this.brandService = brandService;
        this.categoryService = categoryService;
    }

    @GetMapping
    public ResponseEntity<List<Brand>> getAllBrand() {
        List<Brand> brands = brandService.findAll();
        return new ResponseEntity<>(brands, HttpStatus.FOUND);
    }

    @GetMapping("/page/{pageNum}")
    public ResponseEntity<?> listByPage(@PathVariable int pageNum,
                                        @RequestParam(required = false) String keyword) {
        Page<Brand> page = brandService.listByPage(pageNum, keyword);
        return new ResponseEntity<>(page, HttpStatus.FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Brand> deleteBand(@PathVariable Integer id) {
        brandService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping
    public ResponseEntity<Brand> newBand(@RequestParam String name,
                                         @RequestPart MultipartFile file,
                                         @RequestParam List<Integer> categoryIds) throws IOException {
        List<Category> categories = categoryService.findAllByIds(categoryIds);

        Brand brand = new Brand();
        brand.setName(name);
        brand.getCategories().addAll(categories);

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        brand.setLogo(fileName);
        Brand savedBrand = brandService.save(brand);

        String uploadDir = "Brand-logo/" + brand.getId();
        FileUploadUtil.cleanDir(uploadDir);
        FileUploadUtil.saveFile(uploadDir, fileName, file);

        return new ResponseEntity<>(savedBrand, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Brand> updateBrand(@PathVariable Integer id,
                                             @RequestParam String name,
                                             @RequestPart MultipartFile file,
                                             @RequestParam List<Integer> categoryIds) throws IOException {
        List<Category> categories =  categoryService.findAllByIds(categoryIds);

        Brand brand = brandService.findById(id).get();
        brand.setName(name);
        brand.getCategories().clear();
        brand.getCategories().addAll(categories);

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        brand.setLogo(fileName);
        Brand updaedBrand = brandService.save(brand);

        String uploadDir = "Brand-logo/" + brand.getId();
        FileUploadUtil.cleanDir(uploadDir);
        FileUploadUtil.saveFile(uploadDir, fileName, file);

        return new ResponseEntity<>(updaedBrand, HttpStatus.ACCEPTED);
    }

    @PostMapping("/check_unique")
    public String isAvailable(@RequestParam String name) {
        return brandService.checkUnique(name);
    }

}
