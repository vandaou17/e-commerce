package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.product.Product;

public interface ProductService extends BaseService<Product> {

}
