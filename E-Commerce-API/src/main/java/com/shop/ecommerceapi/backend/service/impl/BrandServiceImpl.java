package com.shop.ecommerceapi.backend.service.impl;


import com.shop.ecommerceapi.backend.repository.BrandRepository;
import com.shop.ecommerceapi.backend.service.BrandService;
import com.shop.ecommerceapi.common.entity.Brand;
import com.shop.ecommerceapi.exception.ConflictException;
import com.shop.ecommerceapi.exception.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrandServiceImpl implements BrandService {

    public static final int BRANDS_PER_PAGE = 10;

    private BrandRepository brandRepository;

    public BrandServiceImpl(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }


    @Override
    public Brand save(Brand value) {
        return brandRepository.save(value);
    }

    @Override
    public Brand update(Brand value, Integer id) {
        Brand brand = brandRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A brand with specified ID was not found"));

        return brandRepository.save(value);
    }

    @Override
    public void deleteById(Integer id) {
        Brand brand = brandRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A brand with the specified ID was not found"));

        brandRepository.deleteById(brand.getId());
    }

    @Override
    public Optional<Brand> findById(Integer id) {
        Brand brand = brandRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A brand with the specified ID was not found"));

        return Optional.ofNullable(brand);
    }

    @Override
    public List<Brand> findAll() {
        if (brandRepository.findAll().isEmpty()
                || brandRepository.findAll().size() == 0)
            throw new NotFoundException("Brand empty list in database");

        List<Brand> brands = brandRepository.findAll();
        return brands.stream().toList();
    }

    @Override
    public Page<Brand> listByPage(int pageNum, String keyword) {
        if (findAll().isEmpty())
            throw new NotFoundException("Brand empty list in database");

        Pageable pageable = PageRequest.of(pageNum - 1, BRANDS_PER_PAGE);
        if (keyword != null)
            return brandRepository.findAllByKeyword(keyword, pageable);

        return brandRepository.findAll(pageable);
    }

    @Override
    public String checkUnique(String name) {
        Brand brand = brandRepository.findByName(name)
                .orElseThrow(() -> new ConflictException("A brand with name was conflict"));

        return "OK";
    }
}
