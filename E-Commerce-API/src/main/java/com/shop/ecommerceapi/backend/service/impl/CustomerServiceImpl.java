package com.shop.ecommerceapi.backend.service.impl;

import com.shop.ecommerceapi.backend.repository.CustomerRepository;
import com.shop.ecommerceapi.backend.service.CustomerService;
import com.shop.ecommerceapi.common.entity.Customer;
import com.shop.ecommerceapi.common.entity.User;
import com.shop.ecommerceapi.exception.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    public static  final int CUSTOMER_PER_PAGE = 10;

    private PasswordEncoder passwordEncoder;

    private CustomerRepository customerRepository;

    public CustomerServiceImpl(PasswordEncoder passwordEncoder, CustomerRepository customerRepository) {
        this.passwordEncoder = passwordEncoder;
        this.customerRepository = customerRepository;
    }

    @Override
    public Page<Customer> listByPage(int pageNum, String keyword) {
        if (customerRepository.findAll().isEmpty())
            throw new NotFoundException("Customer empty list in database");

        Pageable pageable = PageRequest.of(pageNum - 1, CUSTOMER_PER_PAGE);
        if (keyword != null)
            return customerRepository.findAllByWord(keyword, pageable);

        return customerRepository.findAll(pageable);
    }

    @Override
    public Optional<Customer> findById(Integer id) {
        if (customerRepository.findById(id).isEmpty())
            throw new NotFoundException("A customer with the specified ID was not found");

        Customer customer = customerRepository.findById(id).get();
        return Optional.of(customer);
    }

    @Override
    public void deleteById(Integer id) {
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A customer with the specified ID was not found"));

        customerRepository.deleteById(customer.getId());
    }

    @Override
    public Customer update(Customer customer, Integer id) {
        Customer oldCustomer = customerRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A customer with the specified ID was not found"));

        oldCustomer.setPassword(passwordEncoder.encode(customer.getPassword()));
        oldCustomer.setFirstName(customer.getFirstName());
        oldCustomer.setLastName(customer.getLastName());
        oldCustomer.setEmail(customer.getEmail());
        oldCustomer.setPhoneNumber(customer.getPhoneNumber());
        oldCustomer.setAddressLine1(customer.getAddressLine1());
        oldCustomer.setAddressLine2(customer.getAddressLine2());
        oldCustomer.setCity(customer.getCity());
        oldCustomer.setCountry(customer.getCountry());
        oldCustomer.setState(customer.getState());
        oldCustomer.setPostalCode(customer.getPostalCode());

        return customerRepository.save(oldCustomer);
    }

}
