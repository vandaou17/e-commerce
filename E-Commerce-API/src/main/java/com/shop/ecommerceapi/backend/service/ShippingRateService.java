package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.ShippingRate;

public interface ShippingRateService extends BaseService<ShippingRate> {

}

