package com.shop.ecommerceapi.backend.service.impl;

import com.shop.ecommerceapi.backend.repository.OrderRepository;
import com.shop.ecommerceapi.backend.service.OrderService;
import com.shop.ecommerceapi.common.entity.order.Order;
import com.shop.ecommerceapi.exception.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    public static final int ORDERS_PER_PAGE = 10;

    private OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Page<Order> listByPage(int pageNum, String keyword) {
        if (orderRepository.findAll().isEmpty() || orderRepository.findAll().size() == 0)
            throw new NotFoundException("Order empty in database");

        Pageable pageable = PageRequest.of(pageNum - 1, ORDERS_PER_PAGE);
        if (keyword != null)
            return orderRepository.findAllByKeyword(keyword, pageable);

        return orderRepository.findAll(pageable);
    }

    @Override
    public void deleteById(Integer id) {
        if(orderRepository.findById(id).isEmpty())
            throw  new NotFoundException("Not found order with specific ID");

        orderRepository.deleteById(id);
    }

    @Override
    public Optional<Order> findById(Integer id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found order with specific ID"));

        return Optional.ofNullable(order);
    }

    @Override
    public Order update(Order value, Integer id) {
        return null;
    }

}
