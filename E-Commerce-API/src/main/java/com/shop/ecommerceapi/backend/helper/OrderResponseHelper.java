package com.shop.ecommerceapi.backend.helper;

import com.shop.ecommerceapi.backend.payload.response.Destination;
import com.shop.ecommerceapi.backend.payload.response.OrderResponse;
import com.shop.ecommerceapi.common.entity.order.Order;
import com.shop.ecommerceapi.common.entity.order.OrderDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class OrderResponseHelper {

    public static Page<OrderResponse> orderResponsePage(Page<Order> page) {

        List<OrderResponse> orderResponse = new ArrayList<>();
        for (Order element : page) {
            Set<OrderDetail> orderDetails = element.getOrderDetails();
            List<Integer> orderDetailsId = new ArrayList<>();
            for (OrderDetail orderdetail : orderDetails) {
                orderDetailsId.add(orderdetail.getId());
            }

            orderResponse.add(new OrderResponse(
                    element.getId(),
                    element.getFirstName() + " " + element.getLastName(),
                    element.getTotal(),
                    element.getOrderTime(),
                    new Destination(element.getCity(), element.getState(), element.getCountry()),
                    element.getPaymentMethod(),
                    element.getStatus(),
                    orderDetailsId,
                    element.getCustomer().getId()));
        }

        Page<OrderResponse> orderResponsePage = new Page<>() {

            @Override
            public int getTotalPages() {
                return page.getTotalPages();
            }

            @Override
            public long getTotalElements() {
                return page.getTotalElements();
            }

            @Override
            public <U> Page<U> map(Function<? super OrderResponse, ? extends U> converter) {
                return null;
            }

            @Override
            public int getNumber() {
                return page.getNumber();
            }

            @Override
            public int getSize() {
                return page.getSize();
            }

            @Override
            public int getNumberOfElements() {
                return page.getNumberOfElements();
            }

            @Override
            public List<OrderResponse> getContent() {
                return orderResponse;
            }

            @Override
            public boolean hasContent() {
                return page.hasContent();
            }

            @Override
            public Sort getSort() {
                return page.getSort();
            }

            @Override
            public boolean isFirst() {
                return page.isFirst();
            }

            @Override
            public boolean isLast() {
                return page.isLast();
            }

            @Override
            public boolean hasNext() {
                return page.hasNext();
            }

            @Override
            public boolean hasPrevious() {
                return page.hasPrevious();
            }

            @Override
            public Pageable nextPageable() {
                return page.nextPageable();
            }

            @Override
            public Pageable previousPageable() {
                return page.previousPageable();
            }

            @Override
            public Iterator<OrderResponse> iterator() {
                return iterator();
            }
        };

        return orderResponsePage;
    }
}
