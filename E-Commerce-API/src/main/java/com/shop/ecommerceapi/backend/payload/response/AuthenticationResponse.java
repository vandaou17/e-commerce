package com.shop.ecommerceapi.backend.payload.response;

public record AuthenticationResponse(String jwtToken) {
}
