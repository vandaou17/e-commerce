package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.Review;
import org.springframework.data.domain.Page;

public interface ReviewService {

    Page<Review> listByPage(int pageNum, String keyword);

    Review findById(Integer id);

    void deleteById(Integer id);

}
