package com.shop.ecommerceapi.backend.service.impl;

import com.shop.ecommerceapi.backend.repository.CategoryRepository;
import com.shop.ecommerceapi.backend.service.CategoryService;
import com.shop.ecommerceapi.common.entity.Category;
import com.shop.ecommerceapi.exception.ConflictException;
import com.shop.ecommerceapi.exception.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CategoryServiceImpl implements CategoryService {

    private static final int ROOT_CATEGORIES_PER_PAGE = 5;

    private CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category save(Category value) {
            Category parent = value.getParent();
            if (parent != null) {
                String allParentIds = parent.getAllParentIds() == null ? "-"
                        : parent.getAllParentIds();
                allParentIds += String.valueOf(parent.getId()) + "-";
                value.setAllParentIds(allParentIds);
            }

        return categoryRepository.save(value);
    }

    @Override
    public Category update(Category value, Integer id) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A category with specified ID was not found"));
        Category parent = category.getParent();
        if (parent != null) {
            String allParentIds = parent.getAllParentIds() == null ? "-"
                    : parent.getAllParentIds();
            allParentIds += String.valueOf(parent.getId()) + "-";
            category.setAllParentIds(allParentIds);
        }

        return categoryRepository.save(category);
    }


    @Override
    public void deleteById(Integer id) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A category with specified ID was not found"));

        categoryRepository.deleteById(category.getId());
    }

    @Override
    public Optional<Category> findById(Integer id) {
        if (categoryRepository.findById(id).isEmpty())
            throw new NotFoundException("A category with specified ID was not found");

        Optional<Category> category = categoryRepository.findById(id);
        return category;
    }

    @Override
    public List<Category> findAll() {
        List<Category> categories = categoryRepository.findAll();
        if (categories.isEmpty() || categories.size() == 0)
            throw new NotFoundException("Categories are empty in database");

        return categories.stream().toList();
    }

    @Override
    public Page<Category> listByPage(int pageNum, String keyword) {
        if (findAll().isEmpty() || findAll().size() == 0)
            throw new NotFoundException("Categories are empty in database");

        Pageable pageable = PageRequest.of(pageNum - 1, ROOT_CATEGORIES_PER_PAGE);
        if (keyword != null || !keyword.isEmpty())
            return categoryRepository.search(keyword, pageable);

        return categoryRepository.findRootCategories(pageable);
    }

    public List<Category> categoriesListInForm() {
        List<Category> categories = new ArrayList<>();

        List<Category> categoriesDB = findAll();
        for (Category category : categoriesDB) {
            categories.add(Category.copyIdAndName(category.getId(), category.getName()));

            Set<Category> children = category.getChildren();

            for (Category child : children) {
                String name = "--" + child.getName();
                categories.add(Category.copyIdAndName(child.getId(), name));

                listChildren(categories, child, 1);
            }
        }

        return categories;
    }

    @Override
    public List<Category> findAllByIds(List<Integer> ids) {
        List<Category> brands = categoryRepository.findAllById(ids);
        if (brands.isEmpty() || brands.size() == 0) {
            throw new NotFoundException("Brand not found with list ids");
        }

        return brands;
    }

    private void listChildren(List<Category> categories, Category parent, int subLevel) {
        int newSubLevel = subLevel + 1;
        Set<Category> children = parent.getChildren();

        for (Category child : children) {
            String name = "";
            for (int i=0; i<newSubLevel; i++)
                name += "--";

            name += child.getName();

            categories.add(Category.copyIdAndName(child.getId(), name));

            listChildren(categories, child, newSubLevel);
        }
    }

    public String checkUnique(String name, String alias) {
        Category categoryByName = categoryRepository.findByName(name).get();
        Category categoryByAlias = categoryRepository.findByAlias(alias).get();
        if (categoryByName != null || categoryByAlias != null)
            return "DuplicateName or Alias";

        return "OK";
    }

}
