package com.shop.ecommerceapi.backend.helper;

import com.shop.ecommerceapi.backend.payload.response.order.OverviewResponse;
import com.shop.ecommerceapi.backend.payload.response.order.ProductResponse;
import com.shop.ecommerceapi.backend.payload.response.order.ShippingResponse;
import com.shop.ecommerceapi.backend.payload.response.order.TrackResponse;
import com.shop.ecommerceapi.common.entity.Customer;
import com.shop.ecommerceapi.common.entity.order.Order;
import com.shop.ecommerceapi.common.entity.order.OrderDetail;
import com.shop.ecommerceapi.common.entity.order.OrderTrack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OrderDetailResponseHelper {

    public static OverviewResponse overview(Order order) {
        String customerName = order.getCustomer().getFirstName() + " " + order.getCustomer().getLastName();
        return  new OverviewResponse(
                order.getId(),
                customerName,
                order.getShippingCost(),
                order.getProductCost(),
                order.getSubTotal(),
                order.getTax(),
                order.getTotal(),
                order.getStatus(),
                order.getPaymentMethod(),
                order.getOrderTime()
        );
    }

    public static Set<ProductResponse> products(Set<OrderDetail> orderDetails) {
        Set<ProductResponse> productResponses = new HashSet<>();
        for (OrderDetail orderDetail : orderDetails) {
            productResponses.add(new ProductResponse(
                    orderDetail.getQuantity(),
                    orderDetail.getProductCost(),
                    orderDetail.getShippingCost(),
                    orderDetail.getUnitPrice(),
                    orderDetail.getSubTotal(),
                    orderDetail.getProduct().getId()
            ));
        }

        return productResponses;
    }

    public static ShippingResponse shipping(Customer customer, Order order) {
        return new ShippingResponse(
                customer.getFirstName(),
                customer.getLastName(),
                customer.getPhoneNumber(),
                customer.getAddressLine1(),
                customer.getAddressLine2(),
                customer.getCity(),
                customer.getState(),
                customer.getPostalCode(),
                customer.getCountry().getName(),
                order.getDeliverDate(),
                order.getDeliverDays()
        );
    }

    public static List<TrackResponse> tracks(List<OrderTrack> orderTracks) {
        List<TrackResponse> trackResponses = new ArrayList<>();
        for (OrderTrack orderTrack : orderTracks) {
            trackResponses.add(new TrackResponse(
                    orderTrack.getStatus(),
                    orderTrack.getNote(),
                    orderTrack.getUpdatedTime()
            ));
        }

        return trackResponses;
    }
}
