package com.shop.ecommerceapi.backend.repository;

import com.shop.ecommerceapi.common.entity.Country;
import com.shop.ecommerceapi.common.entity.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {

    List<State> findByCountryOrderByNameAsc(Country country);
}
