package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.helper.ReviewHelperResponse;
import com.shop.ecommerceapi.backend.payload.response.ReviewResponse;
import com.shop.ecommerceapi.backend.service.ReviewService;
import com.shop.ecommerceapi.common.entity.Review;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/reviews")
public class ReviewRestController {

    private ReviewService reviewService;

    public ReviewRestController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping("/page/{pageNum}")
    public ResponseEntity<?> listByPage(@PathVariable int pageNum,
                                            @RequestParam(required = false) String keyword) {
        Page<Review> reviews = reviewService.listByPage(pageNum, keyword);

        Page<ReviewResponse> responsesPage = ReviewHelperResponse.reviewResponses(reviews);

        return new ResponseEntity<>(responsesPage, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReviewResponse> viewDetail(@PathVariable Integer id) {
        Review review = reviewService.findById(id);

        ReviewResponse reviewResponse = new ReviewResponse(
                review.getId(),
                review.getProduct().getName(),
                review.getCustomer().getFirstName() + " " + review.getCustomer().getLastName(),
                review.getHeadLine(),
                review.getRating(),
                review.getComment(),
                review.getReviewTime()
        );

        return new ResponseEntity<>(reviewResponse, HttpStatus.FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteReview(@PathVariable Integer id) {
        reviewService.deleteById(id);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
