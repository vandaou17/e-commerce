package com.shop.ecommerceapi.backend.payload.response.order;

import com.shop.ecommerceapi.common.entity.order.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrackResponse {

    private OrderStatus status;

    private String note;

    private Date time;
}
