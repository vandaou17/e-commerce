package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.payload.request.ShippingRateRequest;
import com.shop.ecommerceapi.backend.service.CountryService;
import com.shop.ecommerceapi.backend.service.ShippingRateService;
import com.shop.ecommerceapi.common.entity.ShippingRate;
import com.shop.ecommerceapi.common.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/shippingrates")
public class ShippingRateRestController {

    private ShippingRateService shippingRateService;

    private CountryService countryService;

    public ShippingRateRestController(ShippingRateService shippingRateService, CountryService countryService) {
        this.shippingRateService = shippingRateService;
        this.countryService = countryService;
    }

    @GetMapping("/page/{pageNum}")
    public ResponseEntity<?> listShipRateByPage(@PathVariable int pageNum,
                                                @RequestParam(required = false) String keyword) {
        Page<ShippingRate> page = shippingRateService.listByPage(pageNum, keyword);

        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ShippingRate> findById(@PathVariable Integer id) {
        ShippingRate shippingRate = shippingRateService.findById(id).get();

        return new ResponseEntity<>(shippingRate, HttpStatus.FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Integer id) {
        shippingRateService.deleteById(id);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @PostMapping
    public ResponseEntity<ShippingRate> addNewShippingRate(@RequestBody ShippingRateRequest shippingRateRequest) {
        ShippingRate shippingRate = new ShippingRate();

        shippingRate.setRate(shippingRateRequest.getRate());
        shippingRate.setDay(shippingRateRequest.getDay());
        shippingRate.setCodSupported(shippingRateRequest.isCodSupported());
        shippingRate.setState(shippingRateRequest.getState());
        shippingRate.setCountry(countryService.findById(shippingRateRequest.getCountryId()).get());

        ShippingRate savedShippingRate = shippingRateService.save(shippingRate);

        return new ResponseEntity<>(savedShippingRate, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ShippingRate> editShippingRate(@PathVariable Integer id,
                                                         @RequestBody ShippingRateRequest shippingRateRequest) {
        ShippingRate shippingRate = shippingRateService.findById(id).get();
        shippingRate.setRate(shippingRateRequest.getRate());
        shippingRate.setDay(shippingRateRequest.getDay());
        shippingRate.setCodSupported(shippingRateRequest.isCodSupported());
        shippingRate.setState(shippingRateRequest.getState());
        shippingRate.setCountry(countryService.findById(shippingRateRequest.getCountryId()).get());

        ShippingRate savedShippingRate = shippingRateService.update(shippingRate, shippingRate.getId());

        return new ResponseEntity<>(savedShippingRate, HttpStatus.ACCEPTED);
    }


}
