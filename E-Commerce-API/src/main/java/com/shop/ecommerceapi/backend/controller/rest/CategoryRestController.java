package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.FileUploadUtil;
import com.shop.ecommerceapi.backend.repository.CategoryRepository;
import com.shop.ecommerceapi.backend.service.CategoryService;
import com.shop.ecommerceapi.common.entity.Category;
import com.shop.ecommerceapi.common.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/categories")
public class CategoryRestController {

    CategoryService categoryService;

    public CategoryRestController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public ResponseEntity<List<Category>> AllCategories() {
        List<Category> categories = categoryService.findAll();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping("/page/{pageNum}")
    public ResponseEntity<?> listByPage(@PathVariable int pageNum,
                                        @RequestParam(required = false) String keyword) {
        Page<Category> page = categoryService.listByPage(pageNum, keyword);

        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<Category> findById(@PathVariable Integer id) {
        Category category = categoryService.findById(id).get();
        return new ResponseEntity<>(category, HttpStatus.FOUND);
    }

    @PostMapping
    public ResponseEntity<Category> createCategory(@RequestParam String name,
                                                   @RequestParam String alias,
                                                   @RequestParam boolean enabled,
                                                   @RequestParam(required = false) Integer parent_Id,
                                                   @RequestPart MultipartFile file) throws IOException {
        Optional<Category> categoryParent = null;
        if (parent_Id != null) categoryParent = categoryService.findById(parent_Id);

        Category category = new Category();
        category.setName(name);
        category.setAlias(alias);
        category.setEnabled(enabled);
        if (categoryParent.isEmpty() || categoryParent == null)
            category.setParent(null);
        else category.setParent(categoryParent.get());

        Category savedCategory;

        if (file.isEmpty()) {
            savedCategory = categoryService.save(category);
        }
        else {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            category.setImage(fileName);
            savedCategory = categoryService.save(category);

            String uploadDir = "category-images/" + category.getId();
            FileUploadUtil.cleanDir(uploadDir);
            FileUploadUtil.saveFile(uploadDir, fileName, file);
        }

        return new ResponseEntity<>(savedCategory, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Category> updateCategory(@PathVariable Integer id,
                                                   @RequestParam String name,
                                                   @RequestParam String alias,
                                                   @RequestParam boolean enabled,
                                                   @RequestParam(required = false) Integer parent_Id,
                                                   @RequestPart MultipartFile file) throws IOException {
        Optional<Category> categoryParent = categoryService.findById(parent_Id);

        Category category = categoryService.findById(id).get();
        category.setName(name);
        category.setAlias(alias);
        category.setEnabled(enabled);

        if (categoryParent.isPresent()) {
            category.setParent(categoryParent.get());
        } else category.setParent(null);

        Category updatedCategory;
        if (file.isEmpty()) {
            updatedCategory = categoryService.update(category, category.getId());
        }
        else {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            category.setImage(fileName);
            updatedCategory = categoryService.update(category, category.getId());

            String uploadDir = "category-images/" + category.getId();
            FileUploadUtil.cleanDir(uploadDir);
            FileUploadUtil.saveFile(uploadDir, fileName, file);
        }

        return new ResponseEntity<>(updatedCategory, HttpStatus.ACCEPTED);
    }

    @GetMapping("/list_form")
    public ResponseEntity<List<Category>> categoriesListInForm() {
        List<Category> categories = categoryService.categoriesListInForm();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Category> deleteUserById(@PathVariable Integer id) {
        categoryService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/check_unique")
    public String isAvailable(@RequestParam String name,
                              @RequestParam String alias) {
        return categoryService.checkUnique(name, alias);
    }

}
