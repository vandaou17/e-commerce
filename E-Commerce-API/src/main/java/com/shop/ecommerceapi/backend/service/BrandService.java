package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.Brand;

public interface BrandService extends BaseService<Brand> {
    String checkUnique(String name);
}
