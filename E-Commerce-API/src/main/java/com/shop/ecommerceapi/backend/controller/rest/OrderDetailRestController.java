package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.helper.OrderDetailResponseHelper;
import com.shop.ecommerceapi.backend.payload.response.OrderDetailResponse;
import com.shop.ecommerceapi.backend.service.OrderDetailService;
import com.shop.ecommerceapi.common.entity.Customer;
import com.shop.ecommerceapi.common.entity.order.Order;
import com.shop.ecommerceapi.common.entity.order.OrderDetail;
import com.shop.ecommerceapi.common.entity.order.OrderTrack;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/orderDetail")
public class OrderDetailRestController {

    private OrderDetailService orderDetailService;


    public OrderDetailRestController(OrderDetailService orderDetailService) {
        this.orderDetailService = orderDetailService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDetailResponse> viewDetail(@PathVariable Integer id) {
        Order order = orderDetailService.viewDetail(id);
        Set<OrderDetail> orderDetails = order.getOrderDetails();
        Customer customer = order.getCustomer();
        List<OrderTrack> orderTracks = order.getOrderTracks();

        OrderDetailResponse orderDetailResponse = new OrderDetailResponse(
                OrderDetailResponseHelper.overview(order),
                OrderDetailResponseHelper.products(orderDetails),
                OrderDetailResponseHelper.shipping(customer, order),
                OrderDetailResponseHelper.tracks(orderTracks)
        );

        return new ResponseEntity<>(orderDetailResponse, HttpStatus.FOUND);
    }
}
