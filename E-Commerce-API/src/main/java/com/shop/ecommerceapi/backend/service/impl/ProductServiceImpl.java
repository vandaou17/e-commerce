package com.shop.ecommerceapi.backend.service.impl;

import com.shop.ecommerceapi.backend.repository.ProductRepository;
import com.shop.ecommerceapi.backend.service.ProductService;
import com.shop.ecommerceapi.common.entity.product.Product;
import com.shop.ecommerceapi.exception.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private static final int PRODUCTS_PER_PAGE = 5;

    private ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product save(Product value) {
        return productRepository.save(value);
    }

    @Override
    public Product update(Product value, Integer id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found with specific ID"));

        return productRepository.save(value);
    }

    @Override
    public void deleteById(Integer id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found with specific ID"));
        productRepository.deleteById(id);
    }

    @Override
    public Optional<Product> findById(Integer id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found with specific ID"));
        return Optional.ofNullable(product);
    }

    @Override
    public List<Product> findAll() {
        List<Product> products = productRepository.findAll();
        if (products.isEmpty() || products.size() ==0)
            throw new NotFoundException("Not found products in database");

        return products.stream().toList();
    }

    @Override
    public Page<Product> listByPage(int pageNum, String keyword) {
        if (findAll().isEmpty())
            throw new NotFoundException("Not found products in database");

        Pageable pageable = PageRequest.of(pageNum - 1, PRODUCTS_PER_PAGE);
        if (keyword != null)
            return productRepository.searchProductsByName(keyword, pageable);

        return productRepository.findAll(pageable);
    }
}
