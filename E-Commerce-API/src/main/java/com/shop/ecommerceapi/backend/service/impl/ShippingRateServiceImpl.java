package com.shop.ecommerceapi.backend.service.impl;

import com.shop.ecommerceapi.backend.repository.ShippingRateRepository;
import com.shop.ecommerceapi.backend.service.ShippingRateService;
import com.shop.ecommerceapi.common.entity.ShippingRate;
import com.shop.ecommerceapi.exception.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShippingRateServiceImpl implements ShippingRateService {

    public static final int RATES_PER_PAGE = 10;

    private ShippingRateRepository shippingRateRepository;

    public ShippingRateServiceImpl(ShippingRateRepository shippingRateRepository) {
        this.shippingRateRepository = shippingRateRepository;
    }

    @Override
    public ShippingRate save(ShippingRate value) {
        return shippingRateRepository.save(value);
    }

    @Override
    public ShippingRate update(ShippingRate value, Integer id) {
        if (shippingRateRepository.findById(id).isEmpty())
            throw new NotFoundException("A shipping rate with the specified ID was not found");

        return shippingRateRepository.save(value);
    }

    @Override
    public void deleteById(Integer id) {
        ShippingRate shippingRate = shippingRateRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A shipping rate with the specified ID was not found"));

        shippingRateRepository.deleteById(shippingRate.getId());
    }

    @Override
    public Optional<ShippingRate> findById(Integer id) {
        ShippingRate shippingRate = shippingRateRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("A shipping rate with the specified ID was not found"));
        return Optional.ofNullable(shippingRate);
    }

    @Override
    public List<ShippingRate> findAll() {
        List<ShippingRate> shippingRates = shippingRateRepository.findAll();

        if (shippingRates.isEmpty() || shippingRates.size() == 0)
            throw new NotFoundException("shipping rate empty in database");

        return shippingRates.stream().toList();
    }

    @Override
    public Page<ShippingRate> listByPage(int pageNum, String keyword) {
        if (findAll().isEmpty())
            throw new NotFoundException("shipping rate empty in database");

        Pageable pageable = PageRequest.of(pageNum - 1, RATES_PER_PAGE);
        if (keyword != null)
            return shippingRateRepository.findAllByWord(keyword, pageable);

        return shippingRateRepository.findAll(pageable);
    }
}
