package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.FileUploadUtil;
import com.shop.ecommerceapi.backend.service.UserService;
import com.shop.ecommerceapi.backend.service.impl.UserServiceImpl;
import com.shop.ecommerceapi.common.entity.Role;
import com.shop.ecommerceapi.common.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/roles")
    public ResponseEntity<List<Role>> allRoles() {
        List<Role> roles = userService.findAllRole();
        return new ResponseEntity<>(roles, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<User>> allUsers() {
        List<User> users = userService.findAll();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/page/{pageNum}")
    public ResponseEntity<?> listUserByPage(@PathVariable int pageNum,
                                            @RequestParam(required = false) String keyword) {
        Page<User> page = userService.listByPage(pageNum, keyword);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Integer id) {
        User user = userService.findById(id).get();
        return new ResponseEntity<>(user, HttpStatus.FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<User> deleteUserById(@PathVariable Integer id) {
        userService.deleteById(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping
    public ResponseEntity<User> createUser(@RequestParam Integer roleId,
                                           @RequestParam String firstName,
                                           @RequestParam String lastName,
                                           @RequestParam String email,
                                           @RequestParam String password,
                                           @RequestParam boolean enabled,
                                           @RequestPart(required = false) MultipartFile file) throws IOException {

        Optional<Role> role = userService.findRoleById(roleId);

        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPassword(password);
        user.setEmail(email);
        user.setEnabled(enabled);
        user.addRole(role.get());

        User savedUser;

        if (file.isEmpty()) {
            savedUser = userService.save(user);
        } else {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            user.setPhoto(fileName);

            savedUser = userService.save(user);

            String uploadDir = "user-photos/" + user.getId();

            FileUploadUtil.cleanDir(uploadDir);
            FileUploadUtil.saveFile(uploadDir, fileName, file);
        }

        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<User> updateUser(@PathVariable Integer id,
                                           @RequestParam Integer roleId,
                                           @RequestParam String firstName,
                                           @RequestParam String lastName,
                                           @RequestParam String email,
                                           @RequestParam String password,
                                           @RequestParam boolean enabled,
                                           @RequestPart(required = false) MultipartFile file) throws IOException {
        Optional<Role> role = userService.findRoleById(roleId);
        User user = userService.findById(id).get();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPassword(password);
        user.setEmail(email);
        user.setEnabled(enabled);
        user.addRole(role.get());

        User updatedUser;

        if (file.isEmpty()) {
            updatedUser = userService.save(user);
        } else {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            user.setPhoto(fileName);

            updatedUser = userService.update(user, user.getId());

            String uploadDir = "user-photos/" + user.getId();

            FileUploadUtil.cleanDir(uploadDir);
            FileUploadUtil.saveFile(uploadDir, fileName, file);
        }

        return new ResponseEntity<>(updatedUser, HttpStatus.ACCEPTED);
    }

}
