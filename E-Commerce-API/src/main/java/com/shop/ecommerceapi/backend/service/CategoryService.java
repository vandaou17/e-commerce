package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.Category;

import java.util.List;

public interface CategoryService extends BaseService<Category> {

    List<Category> categoriesListInForm();

    List<Category> findAllByIds(List<Integer> ids);

    String checkUnique(String name, String alias);
}
