package com.shop.ecommerceapi.backend.helper;

import com.shop.ecommerceapi.backend.FileUploadUtil;
import com.shop.ecommerceapi.common.entity.product.Product;
import com.shop.ecommerceapi.common.entity.product.ProductDetail;
import com.shop.ecommerceapi.common.entity.product.ProductImage;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Iterator;

public class ProductSaveHelper {

    public static void addMainImageName(MultipartFile file, Product product) throws IOException {
        if (!file.isEmpty()) {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            product.setMainImage(fileName);

            String uploadDir = "product-images/" + product.getId();
            FileUploadUtil.cleanDir(uploadDir);
            FileUploadUtil.saveFile(uploadDir, fileName, file);
        }
    }

    public static void addExtraImages(MultipartFile[] files, Product product) throws IOException {
        if (files.length > 0) {
            for (MultipartFile file : files) {
                if (!file.isEmpty()) {
                    String fileName = StringUtils.cleanPath(file.getOriginalFilename());
                    ProductImage productImage = new ProductImage();
                    productImage.setName(fileName);
                    productImage.setProduct(product);

                    product.getImages().add(productImage);

                    String uploadDir = "product-images-extra/" + productImage.getId();
                    FileUploadUtil.cleanDir(uploadDir);
                    FileUploadUtil.saveFile(uploadDir, fileName, file);
                }
            }
        }
    }

    public static void addProductDetail(String[] detailName, String[] detailValue,
                                        Product product) {
        if (detailName == null || detailName.length == 0) return;

        for (int i=0; i<detailName.length; i++) {
            String name = detailName[i];
            String value = detailValue[i];

            ProductDetail productDetail = new ProductDetail();
            productDetail.setName(name);
            productDetail.setValue(value);
            productDetail.setProduct(product);

            product.getDetails().add(productDetail);
        }
    }
}
