package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.payload.request.CustomerRequest;
import com.shop.ecommerceapi.backend.repository.CustomerRepository;
import com.shop.ecommerceapi.backend.service.CountryService;
import com.shop.ecommerceapi.backend.service.CustomerService;
import com.shop.ecommerceapi.common.entity.Country;
import com.shop.ecommerceapi.common.entity.Customer;
import com.shop.ecommerceapi.common.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/customer")
public class CustomerRestController {

    private CustomerService customerService;

    private CountryService countryService;

    public CustomerRestController(CustomerService customerService, CountryService countryService) {
        this.customerService = customerService;
        this.countryService = countryService;
    }

    @GetMapping("/page/{pageNum}")
    public ResponseEntity<?> listCustomerByPage(@PathVariable int pageNum,
                                            @RequestParam(required = false) String keyword) {
        Page<Customer> page = customerService.listByPage(pageNum, keyword);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> findById(@PathVariable Integer id) {
        Customer customer = customerService.findById(id).get();

        return new ResponseEntity<>(customer, HttpStatus.FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Customer> deleteById(@PathVariable Integer id) {
        customerService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Customer> updateCustomer(@RequestBody CustomerRequest customerRequest,
                                                   @PathVariable Integer id) {

        Customer customer = new Customer();

        Country country = countryService.findById(customerRequest.getCountryId()).get();

        customer.setId(id);
        customer.setFirstName(customerRequest.getFirstName());
        customer.setLastName(customerRequest.getLastName());
        customer.setEmail(customerRequest.getEmail());
        customer.setPassword(customerRequest.getPassword());
        customer.setPhoneNumber(customerRequest.getPhoneNumber());
        customer.setAddressLine1(customerRequest.getAddressLine1());
        customer.setAddressLine2(customerRequest.getAddressLine2());
        customer.setCity(customerRequest.getCity());
        customer.setCountry(country);
        customer.setState(customerRequest.getState());
        customer.setPostalCode(customerRequest.getPostalCode());

        Customer updatedCustomer = customerService.update(customer, id);

        return new ResponseEntity<>(updatedCustomer, HttpStatus.ACCEPTED);
    }

}
