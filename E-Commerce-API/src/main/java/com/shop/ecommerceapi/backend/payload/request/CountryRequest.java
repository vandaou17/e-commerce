package com.shop.ecommerceapi.backend.payload.request;

import lombok.Data;

@Data
public class CountryRequest {

    private String name;

    private String code;
}
