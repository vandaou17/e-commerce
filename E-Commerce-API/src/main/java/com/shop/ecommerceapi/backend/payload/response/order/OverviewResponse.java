package com.shop.ecommerceapi.backend.payload.response.order;

import com.shop.ecommerceapi.common.entity.order.OrderStatus;
import com.shop.ecommerceapi.common.entity.order.PaymentMethod;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OverviewResponse {

    private int id;

    private String customerName;

    private float shippingCost;

    private float productCost;

    private float subTotal;

    private float tax;

    private float total;

    private OrderStatus status;

    private PaymentMethod paymentMethod;

    private Date orderDate;

}
