package com.shop.ecommerceapi.backend.service.impl;

import com.shop.ecommerceapi.backend.repository.CountryRepository;
import com.shop.ecommerceapi.backend.service.CountryService;
import com.shop.ecommerceapi.common.entity.Country;
import com.shop.ecommerceapi.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public Country save(Country value) {
        return countryRepository.save(value);
    }

    @Override
    public Country update(Country value, Integer id) {
        Country country = countryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Cannot delete with specific ID"));

        return countryRepository.save(value);
    }

    @Override
    public void deleteById(Integer id) {
        Country country = countryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Cannot delete with specific ID"));

        countryRepository.deleteById(id);
    }

    @Override
    public Optional<Country> findById(Integer id) {
        Country country = countryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Cannot delete with specific ID"));

        return Optional.ofNullable(country);
    }

    @Override
    public List<Country> findAll() {
        List<Country> countries = countryRepository.findAll();
        if (countries.isEmpty() || countries.size() == 0)
            throw new NotFoundException("Country empty in database");

        return countries;
    }

}
