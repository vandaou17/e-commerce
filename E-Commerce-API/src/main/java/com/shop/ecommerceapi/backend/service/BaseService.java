package com.shop.ecommerceapi.backend.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface BaseService<T> {

    T save(T value);

    T update(T value, Integer id);

    default T delete(T value) {
        return value;
    };

    void deleteById(Integer id);

    Optional<T> findById(Integer id);

    List<T> findAll();

    default  T update(T value){
        return value;
    }

    default Page<T> listByPage(int pageNum, String keyword) {
        return null;
    }

}
