package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.State;

import java.util.List;

public interface StateService {

    List<State> findByCountry(Integer countryId);

    void deleteById(Integer id);

    State save(State value);

    State findById(Integer id);
}
