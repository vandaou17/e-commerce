package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.Role;
import com.shop.ecommerceapi.common.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService extends BaseService<User>{

    List<Role> findAllRole();

    Optional<Role> findRoleById(Integer id);

    User findByEmail(String email);

}
