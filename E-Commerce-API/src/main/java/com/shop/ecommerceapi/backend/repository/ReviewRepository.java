package com.shop.ecommerceapi.backend.repository;

import org.springframework.data.domain.Page;

import com.shop.ecommerceapi.common.entity.Review;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer> {

    @Query("SELECT r FROM Review r WHERE r.headLine LIKE %?1% OR " +
            "r.comment LIKE %?1% OR r.product.name LIKE %?1% OR " +
            "CONCAT(r.customer.firstName, ' ', r.customer.lastName) LIKE %?1%")
    Page<Review> findAllByKeyword(String keyword, Pageable pageable);
}
