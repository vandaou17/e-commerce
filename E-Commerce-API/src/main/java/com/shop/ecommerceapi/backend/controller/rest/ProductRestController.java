package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.helper.ProductSaveHelper;
import com.shop.ecommerceapi.backend.service.BrandService;
import com.shop.ecommerceapi.backend.service.CategoryService;
import com.shop.ecommerceapi.backend.service.ProductService;
import com.shop.ecommerceapi.common.entity.Brand;
import com.shop.ecommerceapi.common.entity.Category;
import com.shop.ecommerceapi.common.entity.product.Product;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductRestController {

    private ProductService productService;

    private BrandService brandService;

    private CategoryService categoryService;

    public ProductRestController(ProductService productService,
                                 BrandService brandService,
                                 CategoryService categoryService) {
        this.productService = productService;
        this.brandService = brandService;
        this.categoryService = categoryService;
    }

    @GetMapping
    public ResponseEntity<List<Product>> allProduct() {
        List<Product> products = productService.findAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> productDetail(@PathVariable Integer id) {
        Product product = productService.findById(id).get();
        return new ResponseEntity<>(product, HttpStatus.FOUND);
    }

    @GetMapping("/page/{pageNum}")
    public ResponseEntity<?> listByPage(@PathVariable Integer pageNum, @RequestParam(required = false) String keyword) {
        Page<Product> page = productService.listByPage(pageNum, keyword);
        return new ResponseEntity<>(page, HttpStatus.FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Product> deleteProduct(@PathVariable Integer id) {
        productService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping
    public ResponseEntity<Product> addProduct(@RequestParam String name,
                                              @RequestParam String alias,
                                              @RequestParam String shortDescription,
                                              @RequestParam String fullDescription,
                                              @RequestParam float cost,
                                              @RequestParam float price,
                                              @RequestParam float disCount,
                                              @RequestParam float length,
                                              @RequestParam float width,
                                              @RequestParam float height,
                                              @RequestParam float weight,
                                              @RequestParam Integer categoryId,
                                              @RequestParam Integer brandId,
                                              @RequestPart MultipartFile mainImage,
                                              @RequestPart MultipartFile[] extraImage,
                                              @RequestParam String[] detailName,
                                              @RequestParam String[] detailValue
                                              ) throws IOException {
        Category category = categoryService.findById(categoryId).get();
        Brand brand = brandService.findById(brandId).get();

        Product product = new Product(name, alias, shortDescription,
                fullDescription, cost, price, disCount, length,
                width, height, weight, category, brand
        );

        ProductSaveHelper.addProductDetail(detailName, detailValue, product);
        ProductSaveHelper.addMainImageName(mainImage, product);
        ProductSaveHelper.addExtraImages(extraImage, product);

        Product savedProduct = productService.save(product);

        return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<Product> editProduct(@PathVariable Integer id,
                                               @RequestParam String name,
                                               @RequestParam String alias,
                                               @RequestParam String shortDescription,
                                               @RequestParam String fullDescription,
                                               @RequestParam float cost,
                                               @RequestParam float price,
                                               @RequestParam float disCount,
                                               @RequestParam float length,
                                               @RequestParam float width,
                                               @RequestParam float height,
                                               @RequestParam float weight,
                                               @RequestParam Boolean inStock,
                                               @RequestParam Boolean enabled,
                                               @RequestParam Integer categoryId,
                                               @RequestParam Integer brandId,
                                               @RequestPart MultipartFile mainImage,
                                               @RequestPart MultipartFile[] extraImage,
                                               @RequestParam String[] detailName,
                                               @RequestParam String[] detailValue
                                               ) throws IOException {
        Category category = categoryService.findById(categoryId).get();
        Brand brand = brandService.findById(brandId).get();

        Product product = productService.findById(id).get();

        product.setName(name);
        product.setAlias(alias);
        product.setShortDescription(shortDescription);
        product.setFullDescription(fullDescription);
        product.setCost(cost);
        product.setPrice(price);
        product.setDiscountPercent(disCount);
        product.setLength(length);
        product.setWidth(width);
        product.setHeight(height);
        product.setWeight(weight);
        product.setUpdatedTime(new Date());
        product.setInStock(inStock);
        product.setEnabled(enabled);
        product.setCategory(category);
        product.setBrand(brand);

        product.getImages().clear();
        product.getDetails().clear();
        ProductSaveHelper.addMainImageName(mainImage, product);
        ProductSaveHelper.addExtraImages(extraImage, product);
        ProductSaveHelper.addProductDetail(detailName, detailValue, product);

        Product updatedProduct = productService.update(product, product.getId());

        return new ResponseEntity<>(updatedProduct, HttpStatus.ACCEPTED);
    }
}
