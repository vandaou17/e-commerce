package com.shop.ecommerceapi.backend.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShippingRateRequest {

    private float rate;

    private int day;

    private boolean codSupported;

    private String state;

    private int countryId;


}
