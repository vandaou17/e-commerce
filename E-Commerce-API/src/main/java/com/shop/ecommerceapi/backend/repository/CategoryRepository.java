package com.shop.ecommerceapi.backend.repository;

import com.shop.ecommerceapi.common.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @Query("SELECT c FROM Category c WHERE c.name LIKE %?1%")
    Page<Category> search(String keyword, Pageable pageable);

    Optional<Category> findByName(String name);

    Optional<Category> findByAlias(String alias);

    @Query("SELECT c FROM Category c WHERE c.parent.id IS NULL")
    Page<Category> findRootCategories(Pageable pageable);
}
