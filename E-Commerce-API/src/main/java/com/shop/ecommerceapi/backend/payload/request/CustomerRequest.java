package com.shop.ecommerceapi.backend.payload.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomerRequest {

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private String phoneNumber;

    private String addressLine1;

    private String addressLine2;

    private String city;

    private int countryId;

    private String state;

    private String postalCode;
}
