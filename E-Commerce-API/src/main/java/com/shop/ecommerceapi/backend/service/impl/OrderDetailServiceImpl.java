package com.shop.ecommerceapi.backend.service.impl;

import com.shop.ecommerceapi.backend.service.OrderDetailService;
import com.shop.ecommerceapi.backend.service.OrderService;
import com.shop.ecommerceapi.common.entity.order.Order;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {

    private OrderService orderService;

    public OrderDetailServiceImpl(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public Order viewDetail(Integer id) {
        return orderService.findById(id).get();
    }
}
