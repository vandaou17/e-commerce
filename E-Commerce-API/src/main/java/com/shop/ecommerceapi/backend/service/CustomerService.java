package com.shop.ecommerceapi.backend.service;

import com.shop.ecommerceapi.common.entity.Customer;
import com.shop.ecommerceapi.common.entity.User;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface CustomerService {

    Page<Customer> listByPage(int pageNum, String keyword);

    Optional<Customer> findById(Integer id);

    void deleteById(Integer id);

    Customer update(Customer customer, Integer id);

}

