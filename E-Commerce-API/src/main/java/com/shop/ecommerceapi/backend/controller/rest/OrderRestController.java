package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.helper.OrderResponseHelper;
import com.shop.ecommerceapi.backend.payload.response.Destination;
import com.shop.ecommerceapi.backend.payload.response.OrderResponse;
import com.shop.ecommerceapi.backend.service.OrderService;
import com.shop.ecommerceapi.common.entity.User;
import com.shop.ecommerceapi.common.entity.order.Order;
import com.shop.ecommerceapi.common.entity.order.OrderDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.function.Function;

@RestController
@RequestMapping("/api/orders")
public class OrderRestController {

    private OrderService orderService;

    public OrderRestController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/page/{pageNum}")
    public ResponseEntity<?> listOrderByPage(@PathVariable int pageNum,
                                            @RequestParam(required = false) String keyword) {
        Page<Order> page = orderService.listByPage(pageNum, keyword);

        Page<OrderResponse> responsePage = OrderResponseHelper.orderResponsePage(page);

        return new ResponseEntity<>(responsePage, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Integer id) {
        orderService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

}
