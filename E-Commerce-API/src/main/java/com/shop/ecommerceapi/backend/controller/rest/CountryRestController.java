package com.shop.ecommerceapi.backend.controller.rest;

import com.shop.ecommerceapi.backend.payload.request.CountryRequest;
import com.shop.ecommerceapi.backend.service.CountryService;
import com.shop.ecommerceapi.common.entity.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryRestController {

    @Autowired
    private CountryService countryService;

    @GetMapping
    public ResponseEntity<List<Country>> findAll() {
        List<Country> countries = countryService.findAll();

        return new ResponseEntity<>(countries, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Country> delete(@PathVariable Integer id) {
        countryService.deleteById(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping
    public ResponseEntity<Country> addCountry(@RequestBody CountryRequest countryRequest) {
        Country country = new Country();
        country.setName(countryRequest.getName());
        country.setCode(countryRequest.getCode());

        Country savedCountry = countryService.save(country);

        return new ResponseEntity<>(savedCountry, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Country> editCountry(@PathVariable Integer id,
                                               @RequestBody CountryRequest countryRequest) {

        Country country = countryService.findById(id).get();
        country.setName(countryRequest.getName());
        country.setCode(countryRequest.getCode());

        Country updatedCountry = countryService.update(country, country.getId());

        return new ResponseEntity<>(updatedCountry, HttpStatus.ACCEPTED);
    }

}
